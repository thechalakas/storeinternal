package com.thechalakas.jay.storeinternal;

/*
 * Created by jay on 20/09/17. 3:46 PM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get the button object
        Button buttonStoreInternal = (Button) findViewById(R.id.buttonStoreInternal);

        Button buttonStoreInternalShow = (Button) findViewById(R.id.buttonStoreInternalShow);

        buttonStoreInternal.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonStoreInternal - clicked");

                String FILENAME = "hello_file";
                String string = "This sentence will be stored in the file";

                //FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                //fos.write(string.getBytes());
                //fos.close();

                try
                {
                    ////get a file stream, write and save.
                    FileOutputStream fileOutputStream = openFileOutput(FILENAME,getApplication().MODE_PRIVATE);
                    fileOutputStream.write(string.getBytes());
                    fileOutputStream.close();
                }
                catch(Exception e)
                {
                    Log.i("MainActivity","Some issue with FileOutputStream --- " + e.toString());
                }


            }
        });

        buttonStoreInternalShow.setOnClickListener(new View.OnClickListener()
        {


            String FILENAME = "hello_file";
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonStoreInternalShow - clicked");
                try
                {
                    /*
                                // Read File and Content
            FileInputStream fin = openFileInput(FILE_NAME);
            int size;
            String neuText = null;

            // read inside if it is not null (-1 means empty)
            while ((size = fin.read()) != -1) {
                // add & append content
                neuText += Character.toString((char) size);
            }
                     */

                    FileInputStream fileInputStream = openFileInput(FILENAME);
                    int size;
                    String readText = "";

                    while ((size = fileInputStream.read()) != -1)
                    {
                        readText += Character.toString((char)size);
                    }
                    Log.i("MainActivity","buttonStoreInternalShow - file contents - " + readText);
                }
                catch(Exception e)
                {
                    Log.i("MainActivity","Some issue with FileOutputStream --- " + e.toString());
                }
            }
        });
    }
}
